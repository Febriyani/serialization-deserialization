﻿using System;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace serialization
{
    [Serializable]
    class Sepatu
    {
        public int NoSepatu;
        public string Merk;
        public string Warna;

        static void Main(string[] args)
        {
            Sepatu obj = new Sepatu();
            obj.NoSepatu = 40;
            obj.Merk = ".Net";

            Sepatu obj2 = new Sepatu();
            obj2.NoSepatu = 39;
            obj2.Merk = ".Net";

            Sepatu obj3 = new Sepatu();
            obj3.NoSepatu = 40;
            obj3.Merk = ".Net";

            IFormatter formatter = new BinaryFormatter();
            Stream stream = new FileStream(@"D:\ExampleNew.txt", FileMode.Create, FileAccess.Write);

            formatter.Serialize(stream, obj);
            formatter.Serialize(stream, obj3);
            stream.Close();

            stream = new FileStream(@"D:\ExampleNew.txt", FileMode.Open, FileAccess.Read);
            Sepatu objnew = (Sepatu)formatter.Deserialize(stream);

            Console.WriteLine(objnew.NoSepatu);
            Console.WriteLine(objnew.Merk);

            Console.ReadKey();


        }
    }
}
